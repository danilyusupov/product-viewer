package com.yusupov.apps.productviewer.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;
    private String category;
    private String name;
    private int price;
    private short rating;

    public Product(String category, String name, int price) {
        this.category = category;
        this.name = name;
        this.price = price;
    }

    public Product(String category, String name, int price, short rating) {
        this.category = category;
        this.name = name;
        this.price = price;
        this.rating = rating;
    }

    public Product(Long id, String category, String name, int price, short rating) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.price = price;
        this.rating = rating;
    }

    public Long getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public short getRating() {
        return rating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setRating(short rating) {
        this.rating = rating;
    }
}
