package com.yusupov.apps.productviewer.service;

import com.yusupov.apps.productviewer.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> findAll();
    void save(Product product);
    void delete(Long id);
    Product findByName(String name);
}
