package com.yusupov.apps.productviewer.repository;

import com.yusupov.apps.productviewer.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findAllByNameIgnoreCase(String name);
    void deleteById(Long id);
    Product findByName(String name);
}
